# Creation of Lambda Function
resource "aws_lambda_function" "collector_nodejs_lambda" {
  function_name = "read_from_s3_Nodejs"
  filename         = "${data.archive_file.nodejs_zip.output_path}"
  source_code_hash = "${data.archive_file.nodejs_zip.output_base64sha256}"
  role    = aws_iam_role.iam_for_lambda.arn
  handler = "nodejs.lambda_handler"
  runtime = "nodejs14.x"
  timeout = 30
  memory_size = 128

  environment {
    variables = {
      greeting = "Hello"
    }
  }
}

# S3 Trigger for Lambda
resource "aws_s3_bucket_notification" "aws-nodejs-lambda-trigger" {
  bucket = aws_s3_bucket.nodejs-bucket.id
  lambda_function {
    lambda_function_arn = aws_lambda_function.collector_nodejs_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_suffix       = ".json"
  }
}
resource "aws_lambda_permission" "invokepermission_nodejs" {
  statement_id  = "AllowS3Invoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.collector_nodejs_lambda.function_name
  principal     = "s3.amazonaws.com"
  source_arn    = "arn:aws:s3:::${aws_s3_bucket.nodejs-bucket.id}"
}