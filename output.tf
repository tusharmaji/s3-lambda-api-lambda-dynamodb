output "rest_api_id" {
  description = "REST API id"
  value       = aws_api_gateway_rest_api.employee-api.id
}
output "deployment_invoke_url" {
  description = "Deployment invoke url"
  #value       = aws_api_gateway_deployment.deployment.invoke_url/${aws_api_gateway_stage.employee-stage.stage_name}/&{aws_api_gateway_resource.gateway.path_part}
  value       = aws_api_gateway_deployment.deployment.invoke_url
}