# S3 Creation
resource "aws_s3_bucket" "nodejs-bucket" {
  bucket_prefix = "lambda-file-api-"
  tags = {
    Name        = "My bucket api"
  }
}
resource "aws_s3_bucket_acl" "example" {
  bucket = aws_s3_bucket.bucket.id
  acl = "private"
}


resource "aws_s3_bucket" "bucket" {
  bucket_prefix = "lambda-file-"
  tags = {
    Name        = "My bucket"
  }
}
resource "aws_s3_bucket_acl" "example1" {
  bucket = aws_s3_bucket.bucket.id
  acl = "private"
}
