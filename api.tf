resource "aws_api_gateway_rest_api" "employee-api" {
  name        = "employee-api"
  description = "AWS Rest API to write to DynamoDB"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_resource" "gateway" {
  rest_api_id = aws_api_gateway_rest_api.employee-api.id
  parent_id   = aws_api_gateway_rest_api.employee-api.root_resource_id
  path_part   = "my-api"
}

resource "aws_api_gateway_method" "post-method" {
  authorization = "NONE"
  http_method   = "POST"
  resource_id   = aws_api_gateway_resource.gateway.id
  rest_api_id   = aws_api_gateway_rest_api.employee-api.id
}

resource "aws_api_gateway_integration" "api-intregation" {
  http_method = aws_api_gateway_method.post-method.http_method
  resource_id = aws_api_gateway_resource.gateway.id
  rest_api_id = aws_api_gateway_rest_api.employee-api.id
  type        = "AWS"
  uri         = aws_lambda_function.api_lambda.invoke_arn
  integration_http_method = "POST"
  passthrough_behavior = "WHEN_NO_TEMPLATES"
  request_templates = {
    "application/json" = ""
  }
}

resource "aws_api_gateway_method_response" "response_200" {
 rest_api_id = aws_api_gateway_rest_api.employee-api.id
 resource_id = aws_api_gateway_resource.gateway.id
 http_method = aws_api_gateway_method.post-method.http_method
 status_code = "200"
 response_models = { "application/json" = "Empty"}
}

resource "aws_api_gateway_integration_response" "IntegrationResponse" {
  depends_on = [
     aws_api_gateway_integration.api-intregation
     ]
  rest_api_id = aws_api_gateway_rest_api.employee-api.id
  resource_id = aws_api_gateway_resource.gateway.id
  http_method = aws_api_gateway_method.post-method.http_method
  status_code = aws_api_gateway_method_response.response_200.status_code
  # Transforms the backend JSON response to json. The space is "A must have"
  response_templates = {
    "application/json" = <<EOF
 
 EOF
 }
}

resource "aws_api_gateway_stage" "employee-stage" {
  deployment_id = aws_api_gateway_deployment.deployment.id
  rest_api_id   = aws_api_gateway_rest_api.employee-api.id
  stage_name    = "employee-stage"
}

resource "aws_api_gateway_deployment" "deployment" {
   depends_on = [
     aws_api_gateway_integration.api-intregation,
     aws_api_gateway_integration_response.IntegrationResponse,
   ]
    rest_api_id = aws_api_gateway_rest_api.employee-api.id
  triggers = {
    redeployment = sha1(jsonencode([
      aws_api_gateway_resource.gateway.id,
      aws_api_gateway_method.post-method.id,
      aws_api_gateway_integration.api-intregation.id,
    ]))
  }
  lifecycle {
    create_before_destroy = true
  }
}
