# Creation of DynamoDB
resource "aws_dynamodb_table" "dynamodb" {
  name             = "dynamodb-test"
  hash_key         = "emp_id"
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5
  attribute {
    name = "emp_id"
    type = "S"
  }
  ttl {
    attribute_name = "TimeToExist"
    enabled        = true
  }
}