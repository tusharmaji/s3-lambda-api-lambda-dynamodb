# Creation of Lambda Function
resource "aws_lambda_function" "api_lambda" {
  function_name = "write_to_dynamodb_lambda_from_API"

  filename         = "${data.archive_file.api_zip.output_path}"
  source_code_hash = "${data.archive_file.api_zip.output_base64sha256}"

  role    = aws_iam_role.iam_for_lambda.arn
  handler = "lambda_2nd.lambda_handler"
  runtime = "python3.6"
  timeout = 30
  memory_size = 128

  environment {
    variables = {
      greeting = "Hello"
    }
  }
}


resource "aws_lambda_permission" "apigw_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.api_lambda.function_name
  principal     = "apigateway.amazonaws.com"
#  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${var.region}:${local.accountId}:${aws_api_gateway_rest_api.employee-api.id}/*/${aws_api_gateway_method.post-method.http_method}${aws_api_gateway_resource.gateway.path}"
}