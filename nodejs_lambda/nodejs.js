const axios = require('axios');
const https = require('https');
const AWS = require('aws-sdk');
exports.handler = async (event,context,callback) => {
    try {
	    const s3 = new AWS.S3();
        const myBucket = event['Records'][0]['s3']['bucket']['name']
        const myKey = event['Records'][0]['s3']['object']['key']
        const params = {Bucket: 'myBucket', Key: 'myKey'};
        const res = await s3.getObject(params).promise(); // await the promise
        const fileContent = res.Body.toString('utf-8');
        console.log(fileContent);
        console.log('API call to call another lambda');
        const url = 'https://16dblc3p5d.execute-api.us-east-1.amazonaws.com/employee-stage/my-api'; // our url
        const response = await axios(
            url, {
                method: 'post',
                data: event,
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                httpsAgent: new https.Agent({
                    rejectUnauthorized: false
                }),
            },
        );
        console.log('Service URL call success');
        console.log(response.data);
        callback(null, response.statusCode);
        //return response;
    } catch (error) {
        console.log('sendAPIRequest() Error: ${error.message}');
        callback(Error(error));
    }
};
