# Creation of Lambda Function
resource "aws_lambda_function" "collector_s3_lambda" {
  function_name = "read_from_s3"

  filename         = "${data.archive_file.s3_zip.output_path}"
  source_code_hash = "${data.archive_file.s3_zip.output_base64sha256}"

  role    = aws_iam_role.iam_for_lambda.arn
  handler = "lambda_1st.lambda_handler"
  runtime = "python3.6"
  timeout = 30
  memory_size = 128

  environment {
    variables = {
      greeting = "Hello"
    }
  }
}

# S3 Trigger for Lambda
resource "aws_s3_bucket_notification" "aws-first-lambda-trigger" {
  bucket = aws_s3_bucket.bucket.id
  lambda_function {
    lambda_function_arn = aws_lambda_function.collector_s3_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_suffix       = ".json"
  }
}
resource "aws_lambda_permission" "invokepermission_1st" {
  statement_id  = "AllowS3Invoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.collector_s3_lambda.function_name
  principal     = "s3.amazonaws.com"
  source_arn    = "arn:aws:s3:::${aws_s3_bucket.bucket.id}"
}