import boto3
import requests
s3_client = boto3.client('s3') 
dynamodb = boto3.resource('dynamodb') 
url = "https://hvg14t9qd7.execute-api.us-east-1.amazonaws.com/employee-stage/my-api"
def lambda_handler(event, context):
    bucket = event['Records'][0]['s3']['bucket']['name'] 
    json_file_name = event['Records'][0]['s3']['object']['key']
    json_object = s3_client.get_object(Bucket=bucket,Key=json_file_name) 
    jsonFileReader = json_object['Body'].read()
    response = requests.post(url, jsonFileReader)
    if (response.status_code == 200):
        print("The request was a success!")
    else:
        print("Result not found!")
    return 'Success'