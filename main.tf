

# Python Code
data "archive_file" "api_zip" {
  type        = "zip"
  source_file = "second_lambda/lambda_2nd.py"
  output_path = "lambda_codes/lambda_2nd.zip"
}

data "archive_file" "s3_zip" {
  type        = "zip"
  source_dir = "first_lambda/"
  output_path = "lambda_codes/lambda_1st.zip"
}

data "archive_file" "nodejs_zip" {
  type        = "zip"
  source_dir = "nodejs_lambda/"
  output_path = "lambda_codes/nodejs.zip"
}

data "aws_caller_identity" "current" {}

locals {
    accountId = data.aws_caller_identity.current.account_id
}